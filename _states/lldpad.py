from __future__ import absolute_import
from salt.exceptions import CommandExecutionError, CommandNotFoundError

import salt.utils

DEFAULT_ON_TLVS = {'TTL', 'chassisID', 'portID', 'sysName'}

def _error(ret, err_msg):
    ret['result'] = False
    ret['comment'] = err_msg
    return ret

def configurenic(name, nic, cfg):
    '''
    Configures a single NIC for LLDPAD.

    This data is usually host specific, at least some of it. So it is best
    rendered in a pillar using host grains to get NIC names and the management
    IP.

    .. code-block:: yaml

        lldpad.configurenic:
            nic: eno1
            cfg:
                lldp: rxtx
                tlvs:
                    mngAddr: {ipv4: "10.10.10.10"}
                    portDesc: ""
                    portID: ""
                    sysCap: ""
                    sysDesc: ""
                    sysName: ""

    nic
        The Network interface to configure
    cfg
        The configuration to apply.
    '''

    ret = {'name': name, 'changes': {}, 'result': None, 'comment': ''}

    try:
        current_cfg = __salt__['lldpad.getconfig'](nic)
    except (CommandNotFoundError, CommandExecutionError) as err:
        ret['result'] = False
        ret['comment'] = 'Error fetching current status \'{0}\': {1}'.format(name, err)
        return ret

    # figure out what we need to change

    # check lldp setting
    curr = current_cfg['lldp']
    want = cfg['lldp']
    if curr != want:
        ret['changes']['lldp'] = "{} -> {}".format(curr, want)

    # quickly normalise "" attrs to {}
    for tlv in cfg['tlvs'].iterkeys():
        if cfg['tlvs'][tlv] == "":
            cfg['tlvs'][tlv] = {}

    # enable all the "always on" TLVs
    for tlv in DEFAULT_ON_TLVS:
        if tlv not in cfg['tlvs']:
            cfg['tlvs'][tlv] = {}

    # check the cfg of enabled TLVs
    changed_tlvs = []
    disabled_tlvs = []
    for tlv, curr_attrs in current_cfg['tlvs'].iteritems():
        # if new state is disabled:
        try:
            want_attrs = cfg['tlvs'][tlv]
            if curr_attrs != want_attrs:
                ret['changes']['tlv {}'.format(tlv)] = '{} -> {}'.format(curr_attrs, want_attrs)
                changed_tlvs.append(tlv)
        except KeyError:
            # it's not currently enabled, and we want it to be
            ret['changes']['tlv {}'.format(tlv)] = '{} -> disabled'.format(curr_attrs)
            disabled_tlvs.append(tlv)

    # check for new TLVs we want enabled
    for tlv, attrs in cfg['tlvs'].iteritems():
        # if we've already covered it
        if tlv in current_cfg['tlvs']:
            continue

        ret['changes']['tlv {}'.format(tlv)] = 'disabled -> {}'.format(attrs)
        changed_tlvs.append(tlv)

    if __opts__['test']:
        ret['result'] = None
        return ret

    if ret['changes'] == {}:
        ret['result'] = True
        ret['comment'] = 'No changes required'
        return ret

    # we're not just testing, so actually make the changes
    change_cfg = {
        'lldp': cfg['lldp'],
        'tlvs': dict(((tlv, cfg['tlvs'][tlv]) for tlv in changed_tlvs)),
        'disable_tlvs': disabled_tlvs
    }

    try:
        __salt__['lldpad.setconfig'](nic, change_cfg)
    except (CommandNotFoundError, CommandExecutionError) as err:
        ret['result'] = False
        ret['comment'] = 'Error fetching current status \'{0}\': {1}'.format(name, err)
        return ret

    ret['result'] = True
    ret['comment'] = 'All changes made successfully'
    return ret
