'''
:maintainer:    Andrew Stewart <code@satook.com>
:maturity:      new
:depends:       None
:platform:      all
'''

import logging
import re

from pipes import quote
import salt.utils as su

log = logging.getLogger(__name__)

TOOL_CMD = 'lldptool'
PARSE_KVP_RE = re.compile('([^=]+)=(.*)')
LLDP_KEY = 'adminStatus'
ENABLED_KEY = 'enableTx'
ENABLED_VAL = 'yes'
DISABLED_VAL = 'no'

def _runcmd(args):
    cmdline = ' '.join((quote(x) for x in args))
    return __salt__['cmd.run'](cmdline)

def getlldp_raw(nic):
    '''
    Returns the configuration status of LLDP tx/rx
    '''
    cmd = [TOOL_CMD, 'get-lldp', '-i', nic, 'adminStatus']
    return _runcmd(cmd)

def setlldp_raw(nic, newcfg):
    '''
    Sets the configuration status of LLDP tx/rx
    '''
    cmd = [TOOL_CMD, 'set-lldp', '-i', nic, 'adminStatus={}'.format(newcfg)]
    return _runcmd(cmd)

TLVS = ['chassisID', 'portID', 'TTL', 'portDesc', 'sysName', 'sysDesc',
 'sysCap', 'mngAddr', 'macPhyCfg', 'powerMdi', 'linkAgg', 'MTU', 'LLDP-MED',
 'medCap', 'medPolicy', 'medLoc', 'medPower', 'medHwRev', 'medFwRev',
 'medSwRev', 'medSerNum', 'medManuf', 'medModel', 'medAssetID', 'CIN-DCBX',
 'CEE-DCBX', 'ETS-CFG', 'ETS-REC', 'PFC', 'APP']

# These tlvs don't need the enableTX=on, so don't send
NO_ENABLE_TLVS = {'TTL', 'chassisID', 'portID', 'TTL'}
DEFAULT_ON_TLVS = {'TTL', 'chassisID', 'portID', 'sysName'}

def gettlv_raw(nic, tlv):
    '''
    Returns the configuration status of a single TLV
    '''
    cmd = [TOOL_CMD, 'get-tlv', '-i', nic, '-V', tlv, '-c']
    return _runcmd(cmd)

def settlv_raw(nic, tlv, attrs):
    '''
    Sets the configuration status for all attributes on a single TLV
    '''

    attrs = ','.join(('{}={}'.format(k,v) for k,v in attrs.items()))
    cmd = [TOOL_CMD, 'set-tlv', '-i', nic, '-V', tlv, attrs]
    return _runcmd(cmd)

def _parseResp(rawstr):
    '''
    Takes the text out from lldptool and turns it into a dict
    '''
    kvps = (PARSE_KVP_RE.match(l.strip()).groups() for l in rawstr.split('\n'))
    return dict(kvps)

def getconfig(nic):
    '''
    Retuns a JSON dict of current configuration of the requested NIC.
    '''
    ret = {
        'ok': True,
        'lldp': _parseResp(getlldp_raw(nic))[LLDP_KEY],
        'tlvs': {}
    }
    for tlv in TLVS:
        try:
            raw_resp = gettlv_raw(nic, tlv)
            cfg = _parseResp(raw_resp)
        except Exception as e:
            ret['tlvs'][tlv] = {'error': raw_resp}
            continue

        enabled = cfg.get(ENABLED_KEY, ENABLED_VAL) == ENABLED_VAL
        if enabled:
            ret['tlvs'][tlv] = dict((k,v) for k,v in cfg.items() if k != ENABLED_KEY)

    return ret

def setconfig(nic, changes):
    '''
    Make changes to the LLDP config as per changes.

    e.g. changes: {
        lldp: 'rxtx'
        tlvs: {
            'sysName': {}
        },
        disable_tlvs: ['TTL', 'mngAddr']
    }
    '''

    ret = {
        'ok': True,
    }
    # set the lldp
    try:
        setlldp_raw(nic, changes['lldp'])
    except KeyError:
        pass

    for tlv, attrs in changes.get('tlvs', {}).iteritems():
        if tlv in NO_ENABLE_TLVS:
            fullattrs = {}
        else:
            fullattrs = {
                ENABLED_KEY: ENABLED_VAL
            }
        fullattrs.update(attrs)

        # we can't change these
        if tlv in DEFAULT_ON_TLVS and fullattrs == {}:
            continue

        settlv_raw(nic, tlv, fullattrs)

    for tlv in changes.get('disable_tlvs', []):
        # we can't change these
        if tlv in DEFAULT_ON_TLVS:
            continue

        settlv_raw(nic, tlv, {ENABLED_KEY: DISABLED_VAL})

    return ret
