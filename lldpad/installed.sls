{% from "lldpad/map.jinja" import map with context %}

lldpad:
  pkg.installed:
    - pkgs: {{ map.pkgs|json }}
  service.running:
    - name: {{ map.service }}
    - enable: True
    - reload: True
