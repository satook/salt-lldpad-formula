# vim: sts=2 ts=2 sw=2 et ai

{% for nic, cfg in salt.pillar.get('lldpad:config', {}).iteritems() %}
lldpad-configure-{{ nic }}:
  lldpad.configurenic:
   - nic: {{ nic }}
   - cfg: {{ cfg }}
{% endfor %}
